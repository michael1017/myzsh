# myzsh

Just a script which setups my zsh environment.

## Usage

copy content in install.sh and run

**NOTE**: The provided one-click installation script will install myzsh to `~/.myzsh`. DO NOT remove this directory because the file `~/.zshrc` is just a symbolic link.
